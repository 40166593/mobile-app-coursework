package com.example.mike.mikebrewcompanion;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Mike on 24/03/2016.
 */
public class MyAdapterRecipe extends ArrayAdapter<Recipe> {

    private final Context context;
    private final List<Recipe> recipeArrayList;

    public MyAdapterRecipe(Context context, List<Recipe> recipeArrayList)
    {
        super(context, R.layout.simplerow, recipeArrayList);

        this.context = context;
        this.recipeArrayList = recipeArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.simplerow, parent, false);

        TextView label = (TextView) rowView.findViewById(R.id.rowTextView);

        label.setText(recipeArrayList.get(position).getName());

        return rowView;
    }

    @Override
    public int getCount()
    {
        return recipeArrayList.size();
    }
}
