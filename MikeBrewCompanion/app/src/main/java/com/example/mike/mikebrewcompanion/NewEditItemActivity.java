package com.example.mike.mikebrewcompanion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class NewEditItemActivity extends AppCompatActivity {

    String mode;
    EditText name;
    EditText description;
    EditText quantity;
    Spinner measurement;
    Button btnAdd;

    private ArrayList<Item> itemList;
    Bundle extras;
    Item editItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_edit_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        extras = getIntent().getExtras();
        if (extras != null)
        {
            mode = extras.getString("MODE");
        }
        else
        {
            mode = "";
        }

        name = (EditText) findViewById(R.id.etName);
        description = (EditText) findViewById(R.id.etDescription);
        quantity = (EditText) findViewById(R.id.etQuantity);
        measurement = (Spinner) findViewById(R.id.spMeasurement);
        btnAdd = (Button) findViewById(R.id.btnAdd);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Measurements, R.layout.support_simple_spinner_dropdown_item);

        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        measurement.setAdapter(adapter);

        try {
            FileInputStream fis = getBaseContext().openFileInput("inventory.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Item> items = (ArrayList<Item>) ois.readObject();
            ois.close();
            fis.close();

            itemList = items;
        } catch (Exception e) {
            Log.d("TEST ", e.getMessage().toString());
        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isValid = true;

                if (name.getText().toString().equals("")) {
                    isValid = false;
                    Toast.makeText(getBaseContext(), "Make a name for the item", Toast.LENGTH_SHORT).show();
                } else if (description.getText().toString().equals("")) {
                    isValid = false;
                    Toast.makeText(getBaseContext(), "Make a description for the item", Toast.LENGTH_SHORT).show();
                } else if (quantity.getText().toString().equals("")) {
                    isValid = false;
                    Toast.makeText(getBaseContext(), "Set a quantity for the item", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    int location = -1;

                    if (mode.equals("edit"))
                    {
                        location = extras.getInt("EDITLOCATION");
                    }

                    for (int i = 0; i < itemList.size(); i++)
                    {
                        if (i != location && name.getText().toString().equals(itemList.get(i).getName()))
                        {
                            isValid = false;
                            Toast.makeText(getBaseContext(), "Please use a unique name", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                try {
                    Double value = Double.parseDouble(quantity.getText().toString());
                } catch (Exception e) {
                    isValid = false;
                    Toast.makeText(getBaseContext(), "Only use numbers and a decimal point", Toast.LENGTH_SHORT).show();
                }

                Item item;

                if (isValid) {
                    item = new Item(name.getText().toString(), description.getText().toString(), Double.parseDouble(quantity.getText().toString()), measurement.getSelectedItem().toString());

                    if (mode.equals("new")) {
                        itemList.add(item);
                    } else {
                        int location = extras.getInt("EDITLOCATION");
                        itemList.set(location, item);
                    }


                    try {
                        FileOutputStream fos = getBaseContext().openFileOutput("inventory.dat", Context.MODE_PRIVATE);
                        ObjectOutputStream oos = new ObjectOutputStream(fos);
                        oos.writeObject(itemList);
                        oos.flush();
                        fos.flush();
                        oos.close();
                        fos.close();
                    } catch (Exception e) {
                        Log.d("error", e.getMessage().toString());
                    }

                    Intent inventory = new Intent(NewEditItemActivity.this, InventoryActivity.class);
                    startActivity(inventory);
                }
            }
        });

        editItem = new Item();

        if (mode.equals("edit"))
        {
            name.setText(itemList.get(extras.getInt("EDITLOCATION")).getName());
            description.setText(itemList.get(extras.getInt("EDITLOCATION")).getDescription());
            quantity.setText(Double.toString(itemList.get(extras.getInt("EDITLOCATION")).getQuantity()));
        }
    }

}
