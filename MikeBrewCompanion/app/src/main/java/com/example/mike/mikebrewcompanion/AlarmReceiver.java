package com.example.mike.mikebrewcompanion;

/*

Code adapted from http://www.concretepage.com/android/android-alarm-clock-tutorial-to-schedule-and-cancel-alarmmanager-pendingintent-and-wakefulbroadcastreceiver-example

 */

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

public class AlarmReceiver extends WakefulBroadcastReceiver {

    Ringtone ringtone;

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d("TEST","TEST");
        Toast.makeText(context, "ALARM", Toast.LENGTH_SHORT).show();
        TimerActivity.getNumberPickerHour().setEnabled(true);
        TimerActivity.getNumberPickerMinute().setEnabled(true);
        TimerActivity.getNumberPickerSecond().setEnabled(true);
        TimerActivity.getNumberPickerHour().setValue(0);
        TimerActivity.getNumberPickerMinute().setValue(0);
        TimerActivity.getNumberPickerSecond().setValue(0);
        TimerActivity.getBtn1().setText(R.string.start);
        TimerActivity.getBtn1().setOnClickListener(TimerActivity.getListener1());
        TimerActivity.getNote().setIsRunning(false);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        ringtone = RingtoneManager.getRingtone(context, uri);
        ringtone.play();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ringtone.stop();
            }
        }, 5000);
    }
}
