package com.example.mike.mikebrewcompanion;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TimerHubActivity extends AppCompatActivity {

    Chronometer chronometer;

    Chronometer.OnChronometerTickListener chronometerTickListener;

    List<Integer> usedIds;

    List<AlarmNotes> alarmNotesList;

    Intent myIntent;
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;

    ListView timerListView;
    MyAdapterTimer timerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer_hub);

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        myIntent = new Intent(TimerHubActivity.this, com.example.mike.mikebrewcompanion.AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(TimerHubActivity.this, 0, myIntent, 0);
        timerListView = (ListView) findViewById(R.id.lvTimers);

        chronometer = (Chronometer) findViewById(R.id.chronometer);

        usedIds = new ArrayList<Integer>();
        alarmNotesList = new ArrayList<AlarmNotes>();

        try
        {
            FileInputStream fis = getBaseContext().openFileInput("timerData.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<AlarmNotes> alarms = (ArrayList<AlarmNotes>) ois.readObject();
            ois.close();
            fis.close();

            alarmNotesList = alarms;
        }
        catch (Exception e)
        {
            Log.d("TEST ", e.getMessage().toString());
        }

        createInterface();
    }

    private void createInterface()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        timerAdapter = new MyAdapterTimer(this, alarmNotesList);
        timerListView.setAdapter(timerAdapter);

        timerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent activity = new Intent(TimerHubActivity.this, TimerActivity.class);
                activity.putExtra("MODE", "edit");
                activity.putExtra("ID", position);
                startActivity(activity);
            }
        });

        timerListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                CharSequence[] options = new CharSequence[1];
                options[0] = "Remove";
                new AlertDialog.Builder(TimerHubActivity.this).setTitle("Title").setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (alarmNotesList.get(position).getIsRunning()) {
                            Toast.makeText(getBaseContext(), "Cannot remove running timer", Toast.LENGTH_SHORT).show();
                        } else {
                            alarmNotesList.remove(position);
                            timerAdapter = new MyAdapterTimer(getBaseContext(), alarmNotesList);
                            timerListView.setAdapter(timerAdapter);

                            try {
                                FileOutputStream fos = getBaseContext().openFileOutput("timerData.dat", Context.MODE_PRIVATE);
                                ObjectOutputStream oos = new ObjectOutputStream(fos);
                                oos.writeObject(alarmNotesList);
                                oos.flush();
                                fos.flush();
                                oos.close();
                                fos.close();
                            } catch (Exception e) {
                                //Toast.makeText(getBaseContext(), "404; file not found", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }).show();
                return true;
            }
        });

        chronometer.start();
        createChronometerListeners();

        chronometer.setOnChronometerTickListener(chronometerTickListener);
    }

    private void createChronometerListeners() {

        chronometerTickListener = new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                Log.d("Second", "passed");
                timerAdapter = new MyAdapterTimer(getBaseContext(), alarmNotesList);
                timerListView.setAdapter(timerAdapter);
                for (int i = 0; i < alarmNotesList.size(); i++)
                {
                    String hourString = "";
                    String minuteString = "";
                    String secondString = "";
                    int secondDifference = 0;
                    int minuteDifference = 0;
                    int hourDifference = 0;

                    if (alarmNotesList.get(i).getIsRunning())
                    {
                        long alarmfulltimeinmillis = alarmNotesList.get(i).getDateTime().getTimeInMillis() / 1000;
                        long currentfulltimeinmillis = Calendar.getInstance().getTimeInMillis() / 1000;

                        int difference = ((int) alarmfulltimeinmillis) - ((int) currentfulltimeinmillis);

                        if (difference >= (60 * 60)) {
                            hourDifference = difference / (60 * 60);
                            difference = difference - (hourDifference * 60 * 60);
                        }
                        if (difference > 60) {
                            minuteDifference = difference / 60;
                            difference = difference - (minuteDifference * 60);
                        }
                        secondDifference = difference;

                        if (alarmfulltimeinmillis <= currentfulltimeinmillis) {
                            alarmNotesList.get(i).setIsRunning(false);
                            alarmNotesList.get(i).setNpHour(0);
                            alarmNotesList.get(i).setNpMinute(0);
                            alarmNotesList.get(i).setNpSecond(0);
                            try {
                                FileOutputStream fos = getBaseContext().openFileOutput("timerData.dat", Context.MODE_PRIVATE);
                                ObjectOutputStream oos = new ObjectOutputStream(fos);
                                oos.writeObject(alarmNotesList);
                                oos.flush();
                                fos.flush();
                                oos.close();
                                fos.close();
                            }
                            catch (Exception e)
                            {

                            }
                        }
                        else
                        {
                            alarmNotesList.get(i).setNpHour(hourDifference);
                            alarmNotesList.get(i).setNpMinute(minuteDifference);
                            alarmNotesList.get(i).setNpSecond(secondDifference);
                        }
                    }
                    else
                    {
                        hourDifference = alarmNotesList.get(i).getNpHour();
                        minuteDifference = alarmNotesList.get(i).getNpMinute();
                        secondDifference = alarmNotesList.get(i).getNpSecond();
                    }
                    hourString = Integer.toString(hourDifference);

                    if (minuteDifference < 10)
                    {
                        minuteString = "0" + Integer.toString(minuteDifference);
                    }
                    else
                    {
                        minuteString = Integer.toString(minuteDifference);
                    }

                    if (secondDifference < 10 && secondDifference > 0)
                    {
                        secondString = "0" + Integer.toString(secondDifference);
                    }
                    else if (secondDifference > 0)
                    {
                        secondString = Integer.toString(secondDifference);
                    }
                    else
                    {
                        secondString = "00";
                    }

                    try
                    {
                        timerAdapter = new MyAdapterTimer(getBaseContext(), alarmNotesList);
                        timerListView.setAdapter(timerAdapter);
                    }
                    catch (Exception e)
                    {
                        Log.d("error: ", e.getMessage().toString());
                    }
                }
                timerAdapter = new MyAdapterTimer(getBaseContext(), alarmNotesList);
                timerListView.setAdapter(timerAdapter);
            }
        };
    }

    @Override
    public void onBackPressed()
    {
        Intent timerActivity = new Intent(TimerHubActivity.this, MainActivity.class);
        startActivity(timerActivity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_bar_itemlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.action_favorite:
                Log.d("SUCCESS", "SUCCESS");
                Intent newItem = new Intent(TimerHubActivity.this, TimerActivity.class);
                newItem.putExtra("MODE", "new");
                startActivity(newItem);
                break;

            case R.id.action_settings:
                Intent intent = new Intent(TimerHubActivity.this, InformationActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }

        return true;
    }
}
