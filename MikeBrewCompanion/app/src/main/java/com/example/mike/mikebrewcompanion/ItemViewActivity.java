package com.example.mike.mikebrewcompanion;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class ItemViewActivity extends AppCompatActivity {

    int location;
    private ArrayList<Item> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        location = 0;
        itemList = new ArrayList<Item>();

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            location = extras.getInt("POSITION");
        }

        try
        {
            FileInputStream fis = getBaseContext().openFileInput("inventory.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Item> items = (ArrayList<Item>) ois.readObject();
            ois.close();
            fis.close();

            itemList = items;
        }
        catch (Exception e)
        {
            Log.d("TEST ", e.getMessage().toString());
        }

        TextView tvName = (TextView) findViewById(R.id.name);
        tvName.setText(itemList.get(location).getName());

        TextView tvDescription = (TextView) findViewById(R.id.description);
        tvDescription.setText(itemList.get(location).getDescription());

        TextView tvValue = (TextView) findViewById(R.id.quantity);
        tvValue.setText(Double.toString(itemList.get(location).getQuantity()) + itemList.get(location).getMeasurement());
    }

}
