package com.example.mike.mikebrewcompanion;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RecipeHubActivity extends AppCompatActivity {

    EditText editTextSearch;
    ListView listViewRecipes;

    MyAdapterRecipe recipeAdapter;

    List<Recipe> recipeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_hub);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recipeList = new ArrayList<>();

        try
        {
            FileInputStream fis = getBaseContext().openFileInput("recipes.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Recipe> fileRecipes = (ArrayList<Recipe>) ois.readObject();
            ois.close();
            fis.close();

            recipeList = fileRecipes;
        }
        catch (Exception e)
        {
            Log.d("TEST ", e.getMessage().toString());
        }

        listViewRecipes = (ListView) findViewById(R.id.lvRecipes);
        recipeAdapter = new MyAdapterRecipe(this, recipeList);
        listViewRecipes.setAdapter(recipeAdapter);

        listViewRecipes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent viewActivity = new Intent(RecipeHubActivity.this, RecipeViewActivity.class);
                viewActivity.putExtra("LOCATION", position);
                startActivity(viewActivity);
            }
        });

        listViewRecipes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                Log.d("Held", Integer.toString(position));
                new AlertDialog.Builder(RecipeHubActivity.this).setTitle("Title").setItems(R.array.dialogOptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == 0) {
                            Intent edit = new Intent(RecipeHubActivity.this, NewEditRecipeActivity.class);
                            edit.putExtra("MODE", "edit");
                            edit.putExtra("EDITLOCATION", position);
                            startActivity(edit);
                        } else if (which == 1) {
                            recipeList.remove(position);
                            recipeAdapter = new MyAdapterRecipe(getBaseContext(), recipeList);

                            listViewRecipes.setAdapter(recipeAdapter);

                            try {
                                FileOutputStream fos = getBaseContext().openFileOutput("recipes.dat", Context.MODE_PRIVATE);
                                ObjectOutputStream oos = new ObjectOutputStream(fos);
                                oos.writeObject(recipeList);
                                oos.flush();
                                fos.flush();
                                oos.close();
                                fos.close();
                            }
                            catch (Exception e)
                            {
                                Log.d("error", e.getMessage().toString());
                            }
                        }
                    }
                }).show();
                return true;
            }
        });

        editTextSearch = (EditText) findViewById(R.id.etSearch);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<Recipe> searchList = new ArrayList<Recipe>();

                for (int i = 0; i < recipeList.size(); i++) {
                    if (recipeList.get(i).getName().contains(s)) {
                        searchList.add(recipeList.get(i));
                    }
                }
                recipeAdapter = new MyAdapterRecipe(getBaseContext(), searchList);
                listViewRecipes.setAdapter(recipeAdapter);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_bar_recipe_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.action_favorite:
                Log.d("SUCCESS", "SUCCESS");
                Intent newItem = new Intent(RecipeHubActivity.this, NewEditRecipeActivity.class);
                newItem.putExtra("MODE", "new");
                startActivity(newItem);
                break;

            case R.id.action_settings:
                Intent intent = new Intent(RecipeHubActivity.this, InformationActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed()
    {
        Intent timerActivity = new Intent(RecipeHubActivity.this, MainActivity.class);
        startActivity(timerActivity);
    }
}
