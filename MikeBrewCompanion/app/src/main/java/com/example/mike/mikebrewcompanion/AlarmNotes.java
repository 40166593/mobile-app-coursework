package com.example.mike.mikebrewcompanion;

import android.app.AlarmManager;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by Mike on 21/03/2016.
 */
public class AlarmNotes implements Serializable {

    private Calendar dateTime; //Time when the alarm will trigger
    private String note; //Note for the alarm
    private int id; //unique id for the alarm
    private boolean isRunning; //check is the alarm should be counting down
    private int npHour; //stores the value in the hour column
    private int npMinute; //stores the value in the minutes column
    private int npSecond; //stores the value in the seconds column

    public void setDateTime(Calendar dateTime) { this.dateTime = dateTime; }

    public Calendar getDateTime()
    {
        return dateTime;
    }

    public void setNote(String note)
    {
        this.note = note;
    }

    public String getNote()
    {
        return note;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public void setIsRunning(boolean isRunning) { this.isRunning = isRunning; }

    public boolean getIsRunning() { return isRunning; }

    public void setNpHour(int npHour) { this.npHour = npHour; }

    public int getNpHour() { return npHour; }

    public void setNpMinute(int npMinute) { this.npMinute = npMinute; }

    public int getNpMinute() { return npMinute; }

    public void setNpSecond(int npSecond) { this.npSecond = npSecond; }

    public int getNpSecond() { return npSecond; }

    public AlarmNotes()
    {

    }

    public AlarmNotes(Calendar calendar, String string, int id, boolean bool)
    {
        dateTime = calendar;
        note = string;
        this.id = id;
        isRunning = bool;
        npHour = 0;
        npMinute = 0;
        npSecond = 0;
    }
}
