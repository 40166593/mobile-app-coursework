package com.example.mike.mikebrewcompanion;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class NewEditRecipeActivity extends AppCompatActivity {

    ToggleButton ingredientsButton;
    ToggleButton timersButton;

    EditText recipeName;
    Button saveButton;

    List<Recipe> recipes;
    Recipe recipe;

    private ListView timerListView;
    private MyAdapterTimer timerListAdapter;

    private ListView ingredientsListView;
    private MyAdapterItem ingredientsAdapter;

    private ListView stepsListView;
    private MyAdapterSteps stepsAdapter;

    AlertDialog myTimerDialog;
    AlertDialog myIngredientDialog;
    AlertDialog myStepsDialog;

    List<Item> itemList;

    Bundle extras;
    String mode;
    int editLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_edit_recipe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        extras = getIntent().getExtras();
        if (extras != null)
        {
            mode = extras.getString("MODE");
        }
        if (mode.equals("edit"))
        {
            editLocation = extras.getInt("EDITLOCATION");
        }
        else
        {
            editLocation = 0;
        }

        recipeName = (EditText) findViewById(R.id.etName);

        recipe = new Recipe();

        recipes = new ArrayList<>();

        try
        {
            FileInputStream fis = getBaseContext().openFileInput("recipes.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Recipe> fileRecipes = (ArrayList<Recipe>) ois.readObject();
            ois.close();
            fis.close();

            recipes = fileRecipes;
        }
        catch (Exception e)
        {
            Log.d("TEST ", e.getMessage().toString());
        }

        if (mode.equals("edit"))
        {
            recipe = recipes.get(editLocation);
            recipeName.setText(recipe.getName());
        }

        ingredientsButton = (ToggleButton) findViewById(R.id.tbIngredients);
        timersButton = (ToggleButton) findViewById(R.id.tbTimer);

        ingredientsListView = (ListView) findViewById(R.id.lvIngredients);
        ingredientsAdapter = new MyAdapterItem(this, recipe.getIngredients());

        ingredientsListView.setAdapter(ingredientsAdapter);
        setListViewHeightBasedOnChildren(ingredientsListView);
        if (recipe.getIngredients().size() != 0)
        {
            ingredientsButton.setChecked(true);
            ingredientsListView.setVisibility(View.VISIBLE);
        }
        else
        {
            ingredientsListView.setVisibility(View.GONE);
        }

        timerListView = (ListView) findViewById(R.id.lvTimer);
        timerListAdapter = new MyAdapterTimer(this, recipe.getTimers());

        timerListView.setAdapter(timerListAdapter);
        setListViewHeightBasedOnChildren(timerListView);
        if (recipe.getTimers().size() != 0)
        {
            timersButton.setChecked(true);
            timerListView.setVisibility(View.VISIBLE);
        }
        else
        {
            timerListView.setVisibility(View.GONE);
        }

        stepsListView = (ListView) findViewById(R.id.lvSteps);
        stepsAdapter = new MyAdapterSteps(this, recipe.getSteps());

        stepsListView.setAdapter(stepsAdapter);
        setListViewHeightBasedOnChildren(stepsListView);

        TextView ingredientLabel = (TextView) findViewById(R.id.tvIngredients);
        TextView timersLabel = (TextView) findViewById(R.id.tvTimers);
        TextView stepsLabel = (TextView) findViewById(R.id.tvSteps);

        ingredientLabel.setText("Ingredients");
        timersLabel.setText("Timers");
        stepsLabel.setText("Steps");

        ingredientsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ingredientsButton.isChecked()) {
                    ingredientsListView.setVisibility(View.VISIBLE);
                } else {
                    ingredientsListView.setVisibility(View.GONE);
                    recipe.setIngredients(new ArrayList<Item>());
                    ingredientsAdapter = new MyAdapterItem(getBaseContext(), recipe.getIngredients());
                    ingredientsListView.setAdapter(ingredientsAdapter);
                }
            }
        });

        timersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timersButton.isChecked())
                {
                    timerListView.setVisibility(View.VISIBLE);
                }
                else
                {
                    timerListView.setVisibility(View.GONE);
                    recipe.setTimers(new ArrayList<AlarmNotes>());
                    timerListAdapter = new MyAdapterTimer(getBaseContext(), recipe.getTimers());
                    timerListView.setAdapter(timerListAdapter);
                }
            }
        });

        saveButton = (Button) findViewById(R.id.btnSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isValid = true;

                if (recipeName.equals(""))
                {
                    isValid = false;
                }
                else if (recipe.getSteps().size() == 0)
                {
                    isValid = false;
                }

                if (isValid)
                {
                    recipe.setName(recipeName.getText().toString());

                    if (mode.equals("edit"))
                    {
                        recipes.set(editLocation, recipe);
                    }
                    else
                    {
                        recipes.add(recipe);
                    }

                    try {
                        FileOutputStream fos = getBaseContext().openFileOutput("recipes.dat", Context.MODE_PRIVATE);
                        ObjectOutputStream oos = new ObjectOutputStream(fos);
                        oos.writeObject(recipes);
                        oos.flush();
                        fos.flush();
                        oos.close();
                        fos.close();
                    }
                    catch (Exception e)
                    {
                        //Toast.makeText(getBaseContext(), "404; file not found", Toast.LENGTH_SHORT).show();
                    }
                    Intent intent = new Intent(NewEditRecipeActivity.this, RecipeHubActivity.class);
                    startActivity(intent);
                }
            }
        });

        timerListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                Log.d("Held", Integer.toString(position));
                new AlertDialog.Builder(NewEditRecipeActivity.this).setTitle("Menu").setItems(R.array.dialogOptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(NewEditRecipeActivity.this);

                            View alertView;

                            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                            alertView = inflater.inflate(R.layout.dialog_create_timer, null);

                            final NumberPicker numberPickerHour = (NumberPicker) alertView.findViewById(R.id.npHour);
                            numberPickerHour.setMaxValue(23);
                            numberPickerHour.setMinValue(0);
                            numberPickerHour.setValue(recipe.getTimers().get(position).getNpHour());
                            setNumberPickerTextColor(numberPickerHour, Color.BLACK);

                            final NumberPicker numberPickerMinute = (NumberPicker) alertView.findViewById(R.id.npMinute);
                            numberPickerMinute.setMaxValue(59);
                            numberPickerMinute.setMinValue(0);
                            numberPickerMinute.setValue(recipe.getTimers().get(position).getNpMinute());
                            numberPickerMinute.setFormatter(new NumberPicker.Formatter() {
                                @Override
                                public String format(int value) {
                                    return String.format("%02d", value);
                                }
                            });
                            setNumberPickerTextColor(numberPickerMinute, Color.BLACK);

                            final NumberPicker numberPickerSecond = (NumberPicker) alertView.findViewById(R.id.npSecond);
                            numberPickerSecond.setMaxValue(59);
                            numberPickerSecond.setMinValue(0);
                            numberPickerSecond.setValue(recipe.getTimers().get(position).getNpSecond());
                            numberPickerSecond.setFormatter(new NumberPicker.Formatter() {
                                @Override
                                public String format(int value) {
                                    return String.format("%02d", value);
                                }
                            });
                            setNumberPickerTextColor(numberPickerSecond, Color.BLACK);

                            final EditText editTextName = (EditText) alertView.findViewById(R.id.et1);
                            editTextName.setText(recipe.getTimers().get(position).getNote());


                            builder.setView(alertView);

                            builder.setTitle("Create timer");
                            builder.setIcon(R.drawable.brew_timer);

                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    AlarmNotes alarmNotes = new AlarmNotes();
                                    alarmNotes.setIsRunning(false);
                                    try {
                                        alarmNotes.setId(recipe.getTimers().size());
                                    } catch (Exception e) {
                                        alarmNotes.setId(0);
                                    }
                                    alarmNotes.setNote(editTextName.getText().toString());
                                    alarmNotes.setNpHour(numberPickerHour.getValue());
                                    alarmNotes.setNpMinute(numberPickerMinute.getValue());
                                    alarmNotes.setNpSecond(numberPickerSecond.getValue());

                                    List<AlarmNotes> alarmNotesList = recipe.getTimers();
                                    alarmNotesList.set(position, alarmNotes);
                                    recipe.setTimers(alarmNotesList);

                                    timerListAdapter = new MyAdapterTimer(getBaseContext(), recipe.getTimers());
                                    timerListView.setAdapter(timerListAdapter);
                                    setListViewHeightBasedOnChildren(timerListView);
                                }
                            });

                            myTimerDialog = builder.create();
                            myTimerDialog.show();
                            myTimerDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);

                            editTextName.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (editTextName.getText().toString().length() != 0) {
                                        myTimerDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                                    } else {
                                        myTimerDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });
                        } else if (which == 1) {
                            recipe.getTimers().remove(position);
                            timerListAdapter = new MyAdapterTimer(getBaseContext(), recipe.getTimers());

                            timerListView.setAdapter(timerListAdapter);
                            setListViewHeightBasedOnChildren(timerListView);
                        }
                    }
                }).show();
                return true;
            }
        });

        ingredientsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                Log.d("Held", Integer.toString(position));
                new AlertDialog.Builder(NewEditRecipeActivity.this).setTitle("Title").setItems(R.array.dialogOptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(NewEditRecipeActivity.this);

                            View alertView;

                            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                            alertView = inflater.inflate(R.layout.dialog_choose_quantity, null);

                            builder.setView(alertView);

                            builder.setTitle("Set quantity");
                            builder.setIcon(R.drawable.brew_inventory);

                            final EditText quantity = (EditText) alertView.findViewById(R.id.etQuantity);
                            quantity.setText(Double.toString(recipe.getIngredients().get(position).getQuantity()));

                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Double value = Double.parseDouble(quantity.getText().toString());
                                    Item newItem = new Item();
                                    newItem.setName(recipe.getIngredients().get(position).getName());
                                    newItem.setMeasurement(recipe.getIngredients().get(position).getMeasurement());
                                    newItem.setQuantity(Double.parseDouble(quantity.getText().toString()));

                                    List<Item> recipeItemList = recipe.getIngredients();
                                    recipeItemList.set(position, newItem);
                                    recipe.setIngredients(recipeItemList);

                                    ingredientsAdapter = new MyAdapterItem(getBaseContext(), recipe.getIngredients());
                                    ingredientsListView.setAdapter(ingredientsAdapter);
                                    setListViewHeightBasedOnChildren(ingredientsListView);
                                }
                            });

                            myIngredientDialog = builder.create();
                            myIngredientDialog.show();
                            myIngredientDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);

                            TextView measurement = (TextView) alertView.findViewById(R.id.tvMeasurement);
                            measurement.setText(recipe.getIngredients().get(position).getMeasurement());

                            quantity.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (quantity.getText().toString().length() != 0) {
                                        try {
                                            Double value = Double.parseDouble(quantity.getText().toString());
                                            myIngredientDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                                        } catch (Exception e) {
                                            myIngredientDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                        }
                                    } else {
                                        myIngredientDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });
                        } else if (which == 1) {
                            recipe.getIngredients().remove(position);
                            ingredientsAdapter = new MyAdapterItem(getBaseContext(), recipe.getIngredients());

                            ingredientsListView.setAdapter(ingredientsAdapter);
                            setListViewHeightBasedOnChildren(ingredientsListView);
                        }
                    }
                }).show();
                return true;
            }
        });

        stepsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                Log.d("Held", Integer.toString(position));
                new AlertDialog.Builder(NewEditRecipeActivity.this).setTitle("Title").setItems(R.array.dialogOptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(NewEditRecipeActivity.this);

                            View alertView;

                            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                            alertView = inflater.inflate(R.layout.dialog_create_recipe_step, null);

                            builder.setView(alertView);

                            builder.setTitle("Create step");
                            builder.setIcon(R.drawable.brew_recipe);

                            final EditText step = (EditText) alertView.findViewById(R.id.etStep);
                            step.setText(recipe.getSteps().get(position));

                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    List<String> stepList = recipe.getSteps();
                                    stepList.set(position, step.getText().toString());
                                    recipe.setSteps(stepList);

                                    stepsAdapter = new MyAdapterSteps(getBaseContext(), recipe.getSteps());
                                    stepsListView.setAdapter(stepsAdapter);
                                    setListViewHeightBasedOnChildren(stepsListView);
                                }
                            });

                            myStepsDialog = builder.create();
                            myStepsDialog.show();
                            myStepsDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);

                            step.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (step.getText().toString().length() != 0) {
                                        myStepsDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                                    } else {
                                        myStepsDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });
                        } else if (which == 1) {
                            recipe.getSteps().remove(position);
                            stepsAdapter = new MyAdapterSteps(getBaseContext(), recipe.getSteps());

                            stepsListView.setAdapter(stepsAdapter);
                            setListViewHeightBasedOnChildren(stepsListView);
                        }
                    }
                }).show();
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_bar_recipe_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.action_favorite:
                Log.d("SUCCESS", "SUCCESS");

                List<CharSequence> options = new ArrayList<>();
                options.add("Add recipe step");

                if (timersButton.isChecked())
                {
                    options.add("Add timer");
                }

                if (ingredientsButton.isChecked())
                {
                    options.add("Add ingredient");
                }

                CharSequence[] optionsArray;

                optionsArray = new CharSequence[options.size()];

                for (int i = 0; i < options.size(); i++)
                {
                    optionsArray[i] = options.get(i);
                }

                new AlertDialog.Builder(NewEditRecipeActivity.this).setTitle("Options").setItems(optionsArray, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == 0) {

                            //ADD RECIPE STEP
                            AlertDialog.Builder builder = new AlertDialog.Builder(NewEditRecipeActivity.this);

                            View alertView;

                            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                            alertView = inflater.inflate(R.layout.dialog_create_recipe_step, null);

                            builder.setView(alertView);

                            builder.setTitle("Create step");
                            builder.setIcon(R.drawable.brew_recipe);

                            final EditText step = (EditText) alertView.findViewById(R.id.etStep);

                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    List<String> stepList = recipe.getSteps();
                                    stepList.add(step.getText().toString());
                                    recipe.setSteps(stepList);

                                    stepsAdapter = new MyAdapterSteps(getBaseContext(), recipe.getSteps());
                                    stepsListView.setAdapter(stepsAdapter);
                                    setListViewHeightBasedOnChildren(stepsListView);
                                }
                            });

                            myStepsDialog = builder.create();
                            myStepsDialog.show();
                            myStepsDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);

                            step.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (count != 0) {
                                        myStepsDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                                    } else {
                                        myStepsDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });

                        } else if (which == 1 && timersButton.isChecked()) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(NewEditRecipeActivity.this);

                            View alertView;

                            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                            alertView = inflater.inflate(R.layout.dialog_create_timer, null);

                            final NumberPicker numberPickerHour = (NumberPicker) alertView.findViewById(R.id.npHour);
                            numberPickerHour.setMaxValue(23);
                            numberPickerHour.setMinValue(0);
                            numberPickerHour.setValue(0);
                            setNumberPickerTextColor(numberPickerHour, Color.BLACK);

                            final NumberPicker numberPickerMinute = (NumberPicker) alertView.findViewById(R.id.npMinute);
                            numberPickerMinute.setMaxValue(59);
                            numberPickerMinute.setMinValue(0);
                            numberPickerMinute.setValue(0);
                            numberPickerMinute.setFormatter(new NumberPicker.Formatter() {
                                @Override
                                public String format(int value) {
                                    return String.format("%02d", value);
                                }
                            });
                            setNumberPickerTextColor(numberPickerMinute, Color.BLACK);

                            final NumberPicker numberPickerSecond = (NumberPicker) alertView.findViewById(R.id.npSecond);
                            numberPickerSecond.setMaxValue(59);
                            numberPickerSecond.setMinValue(0);
                            numberPickerSecond.setValue(0);
                            numberPickerSecond.setFormatter(new NumberPicker.Formatter() {
                                @Override
                                public String format(int value) {
                                    return String.format("%02d", value);
                                }
                            });
                            setNumberPickerTextColor(numberPickerSecond, Color.BLACK);

                            final EditText editTextName = (EditText) alertView.findViewById(R.id.et1);

                            builder.setView(alertView);

                            builder.setTitle("Create timer");
                            builder.setIcon(R.drawable.brew_timer);

                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    AlarmNotes alarmNotes = new AlarmNotes();
                                    alarmNotes.setIsRunning(false);
                                    try {
                                        alarmNotes.setId(recipe.getTimers().size());
                                    } catch (Exception e) {
                                        alarmNotes.setId(0);
                                    }
                                    alarmNotes.setNote(editTextName.getText().toString());
                                    alarmNotes.setNpHour(numberPickerHour.getValue());
                                    alarmNotes.setNpMinute(numberPickerMinute.getValue());
                                    alarmNotes.setNpSecond(numberPickerSecond.getValue());

                                    List<AlarmNotes> alarmNotesList = recipe.getTimers();
                                    alarmNotesList.add(alarmNotes);
                                    recipe.setTimers(alarmNotesList);

                                    timerListAdapter = new MyAdapterTimer(getBaseContext(), recipe.getTimers());
                                    timerListView.setAdapter(timerListAdapter);
                                    setListViewHeightBasedOnChildren(timerListView);
                                }
                            });

                            myTimerDialog = builder.create();
                            myTimerDialog.show();
                            myTimerDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);

                            editTextName.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    if (count != 0) {
                                        myTimerDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                                    } else {
                                        myTimerDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                    }
                                }

                                @Override
                                public void afterTextChanged(Editable s) {

                                }
                            });

                        } else if (which == 1 || which == 2) {

                            //ADD INGREDIENT
                            itemList = new ArrayList<Item>();

                            try {
                                FileInputStream fis = getBaseContext().openFileInput("inventory.dat");
                                ObjectInputStream ois = new ObjectInputStream(fis);
                                ArrayList<Item> items = (ArrayList<Item>) ois.readObject();
                                ois.close();
                                fis.close();

                                itemList = items;
                            } catch (Exception e) {
                                Log.d("TEST ", e.getMessage().toString());
                            }

                            CharSequence[] itemOptions = new CharSequence[itemList.size()];

                            for (int i = 0; i < itemList.size(); i++) {
                                itemOptions[i] = itemList.get(i).getName();
                            }

                            new AlertDialog.Builder(NewEditRecipeActivity.this).setTitle("Choose ingredient").setItems(itemOptions, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    final int selected = which;

                                    AlertDialog.Builder builder = new AlertDialog.Builder(NewEditRecipeActivity.this);

                                    View alertView;

                                    LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                                    alertView = inflater.inflate(R.layout.dialog_choose_quantity, null);

                                    builder.setView(alertView);

                                    builder.setTitle("Set quantity");
                                    builder.setIcon(R.drawable.brew_inventory);

                                    final EditText quantity = (EditText) alertView.findViewById(R.id.etQuantity);

                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            Double value = Double.parseDouble(quantity.getText().toString());
                                            Item newItem = new Item();
                                            newItem.setName(itemList.get(selected).getName());
                                            newItem.setMeasurement(itemList.get((selected)).getMeasurement());
                                            newItem.setQuantity(Double.parseDouble(quantity.getText().toString()));

                                            List<Item> recipeItemList = recipe.getIngredients();
                                            recipeItemList.add(newItem);
                                            recipe.setIngredients(recipeItemList);

                                            ingredientsAdapter = new MyAdapterItem(getBaseContext(), recipe.getIngredients());
                                            ingredientsListView.setAdapter(ingredientsAdapter);
                                            setListViewHeightBasedOnChildren(ingredientsListView);
                                        }
                                    });

                                    myIngredientDialog = builder.create();
                                    myIngredientDialog.show();
                                    myIngredientDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);

                                    TextView measurement = (TextView) alertView.findViewById(R.id.tvMeasurement);
                                    measurement.setText(itemList.get(which).getMeasurement());

                                    quantity.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                        }

                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                                            if (count != 0) {
                                                try {
                                                    Double value = Double.parseDouble(quantity.getText().toString());
                                                    myIngredientDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                                                } catch (Exception e) {
                                                    myIngredientDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                                }
                                            } else {
                                                myIngredientDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                            }
                                        }

                                        @Override
                                        public void afterTextChanged(Editable s) {

                                        }
                                    });
                                }
                            }).show();

                        }
                    }
                }).show();
                break;

            case R.id.action_settings:
                Intent intent = new Intent(NewEditRecipeActivity.this, InformationActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }

        return true;
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color)
    {
        /*
        http://stackoverflow.com/questions/22962075/change-the-text-color-of-numberpicker
         */
        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText){
                try{
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText)child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                }
                catch(NoSuchFieldException e){
                    Log.w("setNPTextColor", e);
                }
                catch(IllegalAccessException e){
                    Log.w("setNPTextColor", e);
                }
                catch(IllegalArgumentException e){
                    Log.w("setNPTextColor", e);
                }
            }
        }
        return false;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        /*
        http://www.java2s.com/Code/Android/UI/setListViewHeightBasedOnChildren.htm
         */
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
