package com.example.mike.mikebrewcompanion;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mike on 24/03/2016.
 */
public class Item implements Serializable {

    private String name; //name of item
    private String description; //description of item
    private Double quantity; //quantity of item expressed as a double
    private String measurement; //measurement of the item, such as g, kg, l, and ml

    public Item() {

        name = "";
        description = "";
        quantity = 0.0;
        measurement = "";

    }

    public Item(String name, String description, Double quantity, String measurement)
    {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.measurement = measurement;
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    public Double getQuantity() { return quantity; }
    public void setQuantity(Double quantity) { this.quantity = quantity; }

    public String getMeasurement() { return measurement; }
    public void setMeasurement(String measurement) { this.measurement = measurement; }
}
