package com.example.mike.mikebrewcompanion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mike on 27/03/2016.
 */
public class Recipe implements Serializable {

    private String name;
    private List<String> steps;
    private List<Item> ingredients;
    private List<AlarmNotes> timers;

    public Recipe()
    {
        steps = new ArrayList<String>();
        ingredients = new ArrayList<Item>();
        timers = new ArrayList<AlarmNotes>();
    }

    public void setName(String name) { this.name = name; }
    public String getName() { return name; }

    public void setSteps(List<String> steps) { this.steps = steps; }
    public List<String> getSteps() { return steps; }

    public void setIngredients(List<Item> ingredients) { this.ingredients = ingredients; }
    public List<Item> getIngredients() { return ingredients; }

    public void setTimers(List<AlarmNotes> timers) { this.timers = timers; }
    public List<AlarmNotes> getTimers() { return timers; }
}
