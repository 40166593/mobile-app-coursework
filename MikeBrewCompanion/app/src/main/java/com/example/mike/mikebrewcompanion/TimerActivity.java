package com.example.mike.mikebrewcompanion;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

public class TimerActivity extends AppCompatActivity {

	AlarmManager alarmManager;
	private PendingIntent pendingIntent;

	static NumberPicker numberPickerHour;
    public static NumberPicker getNumberPickerHour() {return numberPickerHour;}
	static NumberPicker numberPickerMinute;
    public static NumberPicker getNumberPickerMinute() {return numberPickerMinute;}
	static NumberPicker numberPickerSecond;
    public static NumberPicker getNumberPickerSecond() {return numberPickerSecond;}

    EditText editTextNote;

    static Chronometer.OnChronometerTickListener chronometerTickListener;

    static OnClickListener listener1;
    public static OnClickListener getListener1() { return listener1; }
    static OnClickListener listener2;
    static Button btn1;
    public static Button getBtn1() { return btn1; }

	int id;
    static AlarmNotes note;
    public static AlarmNotes getNote() { return note; }
	Intent myIntent;

    String mode;

    List<AlarmNotes> alarmNotesList;

    Chronometer chronometer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

		id = 0;

        note = new AlarmNotes();

        alarmNotesList = new ArrayList<>();

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            id = extras.getInt("ID");
            mode = extras.getString("MODE");
        }

        try
        {
            FileInputStream fis = getBaseContext().openFileInput("timerData.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            List<AlarmNotes> alarmNotes = (ArrayList<AlarmNotes>) ois.readObject();
            ois.close();
            fis.close();

            if (mode.equals("edit"))
            {
                note = alarmNotes.get(id);
            }

            alarmNotesList = alarmNotes;
        }
        catch (Exception e)
        {
            Log.d("TEST ", e.getMessage().toString());
        }

        chronometer = (Chronometer) findViewById(R.id.chromo);
        chronometer.start();

		Toolbar toolbar  = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent timerActivity = new Intent(TimerActivity.this, TimerHubActivity.class);
                startActivity(timerActivity);
            }
        });
		numberPickerHour = (NumberPicker) findViewById(R.id.np1);
		numberPickerMinute = (NumberPicker) findViewById(R.id.np2);
		numberPickerSecond = (NumberPicker) findViewById(R.id.np3);

		numberPickerHour.setMinValue(0);
		numberPickerHour.setMaxValue(23);

		numberPickerMinute.setMinValue(0);
		numberPickerMinute.setMaxValue(59);
        numberPickerMinute.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });

		numberPickerSecond.setMinValue(0);
		numberPickerSecond.setMaxValue(59);
        numberPickerSecond.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.format("%02d", value);
            }
        });

        editTextNote = (EditText) findViewById(R.id.et1);
		
		alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        myIntent = new Intent(TimerActivity.this, com.example.mike.mikebrewcompanion.AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(TimerActivity.this, 0, myIntent, 0);

		btn1 = (Button)findViewById(R.id.button1);

        listener1 = new OnClickListener() {
			public void onClick(View view) {
				Log.d("test", "test");
				setAlarm(numberPickerHour.getValue(), numberPickerMinute.getValue(), numberPickerSecond.getValue());
				btn1.setOnClickListener(listener2);
                btn1.setText(R.string.pause);
			}
         };
         listener2 = new OnClickListener() {
			public void onClick(View view) {
				cancelAlarm();
				btn1.setOnClickListener(listener1);
                btn1.setText(R.string.start);
			}
         };

        if (note.getIsRunning())
        {
            btn1.setOnClickListener(listener2);
            btn1.setText(R.string.pause);
            numberPickerHour.setEnabled(false);
            numberPickerMinute.setEnabled(false);
            numberPickerSecond.setEnabled(false);
        }
        else
        {
            btn1.setOnClickListener(listener1);
            btn1.setText(R.string.start);
        }

        try
        {
            numberPickerHour.setValue(note.getNpHour());
            numberPickerMinute.setValue(note.getNpMinute());
            numberPickerSecond.setValue(note.getNpSecond());
            editTextNote.setText(note.getNote().toString());
        }
        catch (Exception e)
        {

        }


        chronometerTickListener = new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                if (note.getIsRunning() == true)
                {

                    long alarmfulltimeinmillis = note.getDateTime().getTimeInMillis() / 1000;
                    long currentfulltimeinmillis = Calendar.getInstance().getTimeInMillis() / 1000;

                    int difference = ((int) alarmfulltimeinmillis) - ((int) currentfulltimeinmillis);

                    int secondDifference = 0;
                    int minuteDifference = 0;
                    int hourDifference = 0;

                    if (difference >= (60*60))
                    {
                        hourDifference = difference/(60*60);
                        difference = difference - (hourDifference*60*60);
                    }
                    if (difference > 60)
                    {
                        minuteDifference = difference/60;
                        difference = difference - (minuteDifference*60);
                    }
                    secondDifference = difference;

                    numberPickerHour.setValue(hourDifference);
                    numberPickerMinute.setValue(minuteDifference);
                    numberPickerSecond.setValue(secondDifference);
                }
            }
        };

        chronometer.setOnChronometerTickListener(chronometerTickListener);
    }

	private void setAlarm(int hour, int minute, int second){

		int totalHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		int totalMinute = Calendar.getInstance().get(Calendar.MINUTE);
		int totalSecond = Calendar.getInstance().get(Calendar.SECOND);

		totalHour = totalHour + hour;
		totalMinute = totalMinute + minute;
		totalSecond = totalSecond + second;

		if (totalSecond >= 60)
		{
			totalMinute++;
			totalSecond = totalSecond % 60;
		}

		if (totalMinute >=60)
		{
			totalHour++;

		}

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, totalHour);
		calendar.set(Calendar.MINUTE, totalMinute);
		calendar.set(Calendar.SECOND, totalSecond);
		numberPickerHour.setEnabled(false);
		numberPickerMinute.setEnabled(false);
		numberPickerSecond.setEnabled(false);

        note = new AlarmNotes(calendar, editTextNote.toString(), id, true);

		pendingIntent = PendingIntent.getBroadcast(TimerActivity.this, id, myIntent, 0);

		alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
	}
    private void cancelAlarm() {
        if (alarmManager!= null) {
        	alarmManager.cancel(pendingIntent);
			Toast.makeText(getBaseContext(), "Alarm canceled", Toast.LENGTH_SHORT).show();
            numberPickerHour.setEnabled(true);
            numberPickerMinute.setEnabled(true);
            numberPickerSecond.setEnabled(true);
            note.setIsRunning(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try
        {
            chronometer.stop();
            note.setNpHour(numberPickerHour.getValue());
            note.setNpMinute(numberPickerMinute.getValue());
            note.setNpSecond(numberPickerSecond.getValue());
            note.setNote(editTextNote.getText().toString());
            note.setId(id);

            if (mode.equals("new"))
            {
                alarmNotesList.add(note);
            }
            else
            {
                alarmNotesList.set(id, note);
            }

            FileOutputStream fos = getBaseContext().openFileOutput("timerData.dat", Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(alarmNotesList);
            oos.flush();
            fos.flush();
            oos.close();
            fos.close();
        }
        catch (Exception e)
        {
            Log.d("test: ", e.getMessage().toString());
        }
    }

    @Override
    public void onBackPressed()
    {
        Intent timerActivity = new Intent(TimerActivity.this, TimerHubActivity.class);
        startActivity(timerActivity);
    }
} 