package com.example.mike.mikebrewcompanion;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RecipeViewActivity extends AppCompatActivity {

    Recipe recipe;
    Bundle extras;

    List<Item> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        extras = getIntent().getExtras();

        try
        {
            FileInputStream fis = getBaseContext().openFileInput("recipes.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Recipe> fileRecipes = (ArrayList<Recipe>) ois.readObject();
            ois.close();
            fis.close();

            recipe = fileRecipes.get(extras.getInt("LOCATION"));
        }
        catch (Exception e)
        {
            Log.d("TEST ", e.getMessage().toString());
        }

        TextView ingredientsLabel = (TextView) findViewById(R.id.tvIngredients);
        ListView ingredientsListView = (ListView) findViewById(R.id.lvIngredients);
        MyAdapterItem ingredientsAdapter = new MyAdapterItem(this, recipe.getIngredients());
        Button ingredientsSubmitButton = (Button) findViewById(R.id.btnIngredientsSubmit);

        TextView timersLabel = (TextView) findViewById(R.id.tvTimers);
        ListView timersListView = (ListView) findViewById(R.id.lvTimers);
        MyAdapterTimer timersAdapter = new MyAdapterTimer(this, recipe.getTimers());
        Button timersSubmitButton = (Button) findViewById(R.id.btnTimersSubmit);

        TextView stepsLabel = (TextView) findViewById(R.id.tvSteps);
        ListView stepsListView = (ListView) findViewById(R.id.lvSteps);
        MyAdapterSteps stepsAdapter = new MyAdapterSteps(this, recipe.getSteps());

        if (recipe.getIngredients().size() != 0)
        {
            itemList = new ArrayList<Item>();

            try
            {
                FileInputStream fis = getBaseContext().openFileInput("inventory.dat");
                ObjectInputStream ois = new ObjectInputStream(fis);
                ArrayList<Item> items = (ArrayList<Item>) ois.readObject();
                ois.close();
                fis.close();

                itemList = items;
            }
            catch (Exception e)
            {
                Log.d("TEST ", e.getMessage().toString());
            }

            ingredientsLabel.setText("Ingredients");
            ingredientsListView.setAdapter(ingredientsAdapter);
            setListViewHeightBasedOnChildren(ingredientsListView);
            ingredientsSubmitButton.setText("Remove ingredients for inventory");
            ingredientsSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean allIngredientsValid = true;

                    for (int i = 0; i < recipe.getIngredients().size(); i++)
                    {
                        boolean isItemValid = false;

                        for (int j = 0; j < itemList.size(); j++)
                        {
                            if (recipe.getIngredients().get(i).getName().equals(itemList.get(j).getName()) && itemList.get(j).getQuantity() >= recipe.getIngredients().get(i).getQuantity())
                            {
                                isItemValid = true;
                            }
                        }

                        if (!isItemValid)
                        {
                            allIngredientsValid = false;
                        }
                    }

                    if(allIngredientsValid)
                    {
                        Toast.makeText(getBaseContext(), "Removing from stock...", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < recipe.getIngredients().size(); i++)
                        {
                            for (int j = 0; j < itemList.size(); j++)
                            {
                                if (recipe.getIngredients().get(i).getName().equals(itemList.get(j).getName()))
                                {
                                    itemList.get(j).setQuantity((itemList.get(j).getQuantity()) - (recipe.getIngredients().get(i).getQuantity()));
                                }
                            }
                        }

                        try {
                            FileOutputStream fos = getBaseContext().openFileOutput("inventory.dat", Context.MODE_PRIVATE);
                            ObjectOutputStream oos = new ObjectOutputStream(fos);
                            oos.writeObject(itemList);
                            oos.flush();
                            fos.flush();
                            oos.close();
                            fos.close();
                        }
                        catch (Exception e)
                        {
                            Log.d("error", e.getMessage().toString());
                        }
                    }
                    else
                    {
                        Toast.makeText(getBaseContext(), "You do not have enough stock to remove", Toast.LENGTH_SHORT).show();
                    }

                    Log.d("IS VALID?", String.valueOf(allIngredientsValid));
                }
            });
        } else
        {
            ingredientsLabel.setVisibility(View.GONE);
            ingredientsListView.setVisibility(View.GONE);
            ingredientsSubmitButton.setVisibility(View.GONE);
        }

        if (recipe.getTimers().size() != 0)
        {
            timersLabel.setText("Timers");
            timersListView.setAdapter(timersAdapter);
            setListViewHeightBasedOnChildren(timersListView);
            timersSubmitButton.setText("Prepare timers");
            timersSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //REWRITE TIMERS TO MATCH IMERS FROM RECIPE
                    try {
                        FileOutputStream fos = getBaseContext().openFileOutput("timerData.dat", Context.MODE_PRIVATE);
                        ObjectOutputStream oos = new ObjectOutputStream(fos);
                        oos.writeObject(recipe.getTimers());
                        oos.flush();
                        fos.flush();
                        oos.close();
                        fos.close();
                    }
                    catch (Exception e) {
                        Log.d("error", e.getMessage().toString());
                    }
                }
            });
        }
        else
        {
            timersLabel.setVisibility(View.GONE);
            timersListView.setVisibility(View.GONE);
            timersSubmitButton.setVisibility(View.GONE);
        }

        stepsLabel.setText("Steps");
        stepsListView.setAdapter(stepsAdapter);
        setListViewHeightBasedOnChildren(stepsListView);

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

}
