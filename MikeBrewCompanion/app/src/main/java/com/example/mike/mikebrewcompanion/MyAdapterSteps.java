package com.example.mike.mikebrewcompanion;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Mike on 24/03/2016.
 */
public class MyAdapterSteps extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> stepArrayList;

    public MyAdapterSteps(Context context, List<String> stepArrayList)
    {
        super(context, R.layout.simplerow, stepArrayList);

        this.context = context;
        this.stepArrayList = stepArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.simplerow, parent, false);

        TextView labelView = (TextView) rowView.findViewById(R.id.rowTextView);
        labelView.setText(stepArrayList.get(position));

        return rowView;
    }

    @Override
    public int getCount()
    {
        return stepArrayList.size();
    }
}
