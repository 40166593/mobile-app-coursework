package com.example.mike.mikebrewcompanion;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mike on 24/03/2016.
 */
public class MyAdapterTimer extends ArrayAdapter<AlarmNotes> {

    private final Context context;
    private final List<AlarmNotes> timerArrayList;

    public MyAdapterTimer(Context context, List<AlarmNotes> timerArrayList)
    {
        super(context, R.layout.customrow, timerArrayList);

        this.context = context;
        this.timerArrayList = timerArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.customrow, parent, false);

        TextView labelView = (TextView) rowView.findViewById(R.id.tvLabel);
        TextView valueView = (TextView) rowView.findViewById(R.id.tvValue);

        labelView.setText(timerArrayList.get(position).getNote());
        valueView.setText(timerArrayList.get(position).getNpHour() + ":" + String.format("%02d",timerArrayList.get(position).getNpMinute()) + ":" + String.format("%02d", timerArrayList.get(position).getNpSecond()));

        return rowView;
    }

    @Override
    public int getCount()
    {
        return timerArrayList.size();
    }
}
