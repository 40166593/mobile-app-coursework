package com.example.mike.mikebrewcompanion;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class InventoryActivity extends AppCompatActivity {

    private ListView mainListView ;
    private MyAdapterItem listAdapter ;
    private ArrayList<Item> itemList;
    private EditText inventorySeach;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Find the ListView resource.
        mainListView = (ListView) findViewById( R.id.mainListView );

        itemList = new ArrayList<Item>();

        try //Attempt to read from file
        {
            FileInputStream fis = getBaseContext().openFileInput("inventory.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Item> items = (ArrayList<Item>) ois.readObject();
            ois.close();
            fis.close();

            itemList = items;
        }
        catch (Exception e)
        {
            Log.d("TEST ", e.getMessage().toString());
        }

        // Create ArrayAdapter using the planet list.
        listAdapter = new MyAdapterItem(this, itemList);

        mainListView.setAdapter(listAdapter);

        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Clicked", Integer.toString(position));
                Intent itemViewer = new Intent(getApplicationContext(), ItemViewActivity.class);
                itemViewer.putExtra("POSITION", position);
                startActivity(itemViewer);
            }
        });

        mainListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                Log.d("Held", Integer.toString(position));
                new AlertDialog.Builder(InventoryActivity.this).setTitle("Title").setItems(R.array.dialogOptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == 0) {
                            Intent edit = new Intent(InventoryActivity.this, NewEditItemActivity.class);
                            edit.putExtra("MODE", "edit");
                            edit.putExtra("EDITLOCATION", position);
                            startActivity(edit);
                        } else if (which == 1) {
                            itemList.remove(position);
                            listAdapter = new MyAdapterItem(getBaseContext(), itemList);

                            mainListView.setAdapter(listAdapter);
                        }
                    }
                }).show();
                return true;
            }
        });

        inventorySeach = (EditText) findViewById(R.id.etSearch);

        inventorySeach.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<Item> searchList = new ArrayList<Item>();

                for(int i = 0; i<itemList.size();i++)
                {
                    if (itemList.get(i).getName().contains(s) || itemList.get(i).getDescription().contains(s))
                    {
                        searchList.add(itemList.get(i));
                    }
                }
                listAdapter = new MyAdapterItem(getBaseContext(), searchList);
                mainListView.setAdapter(listAdapter);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_bar_itemlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.action_favorite:
                Log.d("SUCCESS", "SUCCESS");
                Intent newItem = new Intent(InventoryActivity.this, NewEditItemActivity.class);
                newItem.putExtra("MODE", "new");
                startActivity(newItem);
                break;

            case R.id.action_settings:
                Intent intent = new Intent(InventoryActivity.this, InformationActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }

        return true;
    }

    @Override
    public void onPause()
    {
        super.onPause();

        try {
            FileOutputStream fos = getBaseContext().openFileOutput("inventory.dat", Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(itemList);
            oos.flush();
            fos.flush();
            oos.close();
            fos.close();
        }
        catch (Exception e)
        {
            //Toast.makeText(getBaseContext(), "404; file not found", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed()
    {
        Intent timerActivity = new Intent(InventoryActivity.this, MainActivity.class);
        startActivity(timerActivity);
    }
}
