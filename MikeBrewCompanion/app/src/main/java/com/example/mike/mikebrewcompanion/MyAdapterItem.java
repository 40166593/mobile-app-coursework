package com.example.mike.mikebrewcompanion;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mike on 24/03/2016.
 */
public class MyAdapterItem extends ArrayAdapter<Item> {

    private final Context context;
    private final List<Item> itemArrayList;

    public MyAdapterItem(Context context, List<Item> itemArrayList)
    {
        super(context, R.layout.customrow, itemArrayList);

        this.context = context;
        this.itemArrayList = itemArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.customrow, parent, false);

        TextView labelView = (TextView) rowView.findViewById(R.id.tvLabel);
        TextView valueView = (TextView) rowView.findViewById(R.id.tvValue);

        labelView.setText(itemArrayList.get(position).getName());
        valueView.setText(Double.toString(itemArrayList.get(position).getQuantity()) + itemArrayList.get(position).getMeasurement());

        return rowView;
    }
}
