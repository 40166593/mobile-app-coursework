# Mike's Brew Companion #

This is my mobile application designed to help with the homebrewing process.

### Main features that have been fully implemented ###

### Main features that have been partially implemented ###
* Timer
### Main features that have not been implemented but are planned ###
* Calendar
* Inventory (reminder when low inventory)
* Recipe Builder (integrate with timer and inventory)
* Calculators (Refractometer http://www.primetab.com/formulas.html)